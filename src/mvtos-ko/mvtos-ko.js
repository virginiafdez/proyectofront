import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

class MvtosKo extends PolymerElement {
static get template() {
  return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      :host {
        display: block;
        background-color: #E8F0F7
      }

      .h1{
        text-align: center
      }
    </style>
    <br><br>
    <h1>No existen movimientos para este contrato</h1><br><br>
    <button class="btn btn-info" on-click="volvercontratos">Volver a Lista</button>
    `;
  }

      static get is() { return 'mvtos-ko'; }
      static get properties() {
        return {
          idcliente:{
            type: Number
          }
        };
      }

      volvercontratos(){
        this.dispatchEvent(new CustomEvent("eventovolvercontratos", {"detail":{}}))
      }
    }

    window.customElements.define('mvtos-ko', MvtosKo);
