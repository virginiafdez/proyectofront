import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../login-usuario/login-usuario.js';
import '../visor-contratos/visor-contratos.js';
import '../contrato-ko/contrato-ko.js';
import '../confirmar-cancelar/confirmar-cancelar.js';
import '../visor-movimientos/visor-movimientos.js';
import '../mvtos-ko/mvtos-ko.js';
import '../registro-usuario/registro-usuario.js';
import '../alta-ok/alta-ok.js';
import '../error-logado/error-logado.js'



  class PadreApp extends PolymerElement {
    static get template() {
      return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
      [hidden]{
        display: none !important;
      }
      :host {
        display: block;
      }

      .pull-right {
        float: right !important;
      }

    </style>

    <iron-ajax id="hacerLogout"
          url="http://localhost:3000/usuarios/logout"
          method="POST"
          handle-as="json"
          on-response="mostrarLogout"
          on-error="mostrarErrorLogout">
    </iron-ajax>

    <button class ="btn btn-primary" hidden$="{{!showlogout}}" on-click="salir" style="position: absolute; top: 3; right: 1;">Logout</button>

    <login-usuario hidden$={{estoygado}} id="lgu" on-eventoregistro="procesaregistro" on-eventologado="procesarlogado" on-eventoerrorlogado="procesarerrorlogado"></login-usuario>
    <error-logado hidden$={{!isloginko}} id="lko" on-eventovolverlogin="procesarvolverlogin"></error-logado>
    <registro-usuario hidden$={{!isregistro}} id="reg" on-eventoaltaok="procesaralta" on-eventovolverlogin="procesarvolverlogin"></registro-usuario>
    <alta-ok hidden$={{!isaltaok}} id="reg" on-eventovolverlogin="procesarvolverlogin"></alta-ok>
    <visor-contratos hidden$={{!iscontrato}} id="vsc" on-contratoconsultado="procesarcontrato" on-eventogetcontratosko="procesarkocontratos" on-eventoconfirmarcancelar="procesarconfirmarborrado"></visor-contratos>
    <!--<visor-contratos hidden$={{!iscontrato}} id="vsc" on-contratoconsultado="procesarcontrato" on-eventoconfirmarcancelar="procesarconfirmarborrado"></visor-contratos>-->
    <contrato-ko hidden$={{!iscontratoko}} id="contko" on-eventologado="procesarlogado"></contrato-ko>
    <confirmar-cancelar hidden$={{!isconfirmarborr}} id="confko" on-contratocancelado="procesarcontratocancelado"></confirmar-cancelar>
    <visor-movimientos hidden$={{!ismovimiento}} id="mov" on-eventogetmvtosko="procesarkomvtos" on-eventovolvercontratos="procesarvolvercontratos"></visor-movimientos>
    <mvtos-ko hidden$={{!ismvtoko}} id="movko" on-eventovolvercontratos="procesarvolvercontratos"></mvtos-ko>
    <!--<logout-usuario hidden$={{!estoylogado}} id="lgo" on-eventologout="procesarlogout"></logout-usuario>-->
    `;
  }
      static get is() { return 'padre-app'; }
      static get properties() {
        return {
          estoylogado:{
            type: Boolean,
            value:false
          },
          isloginko:{
            type: Boolean,
            value: false
          },
          idcliente:{
            type: String
          },
          iscontrato:{
            type: Boolean,
            value:false
          },
          ismovimiento:{
            type: Boolean,
            value:false
          },
          isregistro:{
            type: Boolean,
            value:false
          },
          isaltaok:{
            type: Boolean,
            value:false
          },
          showlogout:{
            type: Boolean,
            value:false
          },
          iscontratoko:{
            type: Boolean,
            value:false
          },
          ismvtoko:{
            type: Boolean,
            value:false
          },
          isconfirmarborr:{
            type: Boolean,
            value:false
          }
        };
      }

      procesarlogado(e){
        var id =  e.detail.id
        this.idcliente = id
        this.$.vsc.estado = "A"
        this.$.vsc.obtenerContratos(id)
        this.estoylogado = true
        this.iscontrato = true
        this.showlogout = true
        this.iscontratoko = false
      }

      procesarerrorlogado(){
        this.estoylogado = true
        this.isloginko = true
      }

      procesaregistro(e){
        this.isregistro = true
        this.estoylogado = true
      }

      procesaralta(e){
        this.isaltaok = true
        this.isregistro = false
      }

      procesarvolverlogin(){
        this.estoylogado = false
        this.isloginko = false
        this.isregistro = false
        this.isaltaok = false
      }

      procesarcontrato(e){
        var iban = e.detail.iban
        this.$.mov.obtenerMovimientos(iban)
        this.ismovimiento = true
        this.iscontrato = false
        this.ismvtoko = false
      }

      procesarkocontratos(e){
        this.iscontrato = false
        this.iscontratoko = true
        this.showlogout = true
      }

      procesarconfirmarborrado(e){
        this.$.confko.iban = e.detail.iban
        this.$.confko.idcliente = this.idcliente
        this.showlogout = false
        this.iscontrato = false
        this.isconfirmarborr = true
      }

      procesarcontratocancelado(e){
        this.isconfirmarborr = false
        var id =  e.detail.idcliente
        this.idcliente = id
        this.$.vsc.estado = "A"
        this.$.vsc.obtenerContratos(id)
        this.estoylogado = true
        this.iscontrato = true
        this.showlogout = true
      }

      procesarkomvtos(e){
        this.ismovimiento = false
        this.ismvtoko = true
      }

      procesarvolvercontratos(){
        this.ismvtoko = false
        this.ismovimiento = false
        this.iscontrato = true
      }

      procesarlogout(e){
        this.estoylogado = false
      }

      salir(e){
        this.$.hacerLogout.headers.id = this.idcliente
        this.$.hacerLogout.generateRequest()
      }

      mostrarLogout(resultado){
        console.log("mostrar logout")
        this.iscontrato = false
        this.ismovimiento = false
        this.estoylogado = false
        this.showlogout = false
        this.iscontratoko = false
        this.ismvtoko = false
      }

    }

    window.customElements.define('padre-app', PadreApp);
