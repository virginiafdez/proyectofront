import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

class ContratoKo extends PolymerElement {
  static get template() {
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      :host {
        display: block;
        background-color: #E8F0F7
      }

      .h1{
        text-align: center
      }
    </style>

    <iron-ajax id="altaContrato"
          url="http://localhost:3000/contratos"
          method="POST"
          handle-as="json"
          on-response="mostrarAltaContrato"
          on-error="mostrarErrorAltaContrato">
    </iron-ajax>

    <br><br>
    <h1>No tiene contratos activos actualmente</h1><br>
    <button class="btn btn-primary" on-click="contratar">Contratar</button>

      `;
    }

      static get is() { return 'contrato-ko'; }
      static get properties() {
        return {
          idcliente:{
            type: Number
          },
          estado:{
            type: String,
            value: "A"
          }
        };
      }

      contratar(){
        this.idcliente = sessionStorage.getItem("idcliente")
        this.$.altaContrato.headers.idcliente = this.idcliente
        this.$.altaContrato.generateRequest()
      }

      mostrarAltaContrato(resultado){
        var idcliente = this.idcliente
        this.dispatchEvent(new CustomEvent("eventologado", {"detail":{"id":idcliente}}))
      }

      mostrarErrorAltaContrato(error){
        this.logado = false
      }
    }

    window.customElements.define('contrato-ko', ContratoKo);
