import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
   <style>
     :host {
       display: block;
     }
   </style>
   <iron-ajax auto="" id="obtenerUsuario"
              url="http://localhost:3000/usuarios/{{id}}"
              handle-as="json"
              on-response="mostrarDatos">

   </iron-ajax>
     <div class="row">
       <h1 class="col-3">{{nombre}}</h1>
       <h2 class="col-3">{{apellido}}</h2>
   </div>
   `;
 }
   /**
    * @customElement
    * @polymer
    */

     static get is() { return 'visor-usuario'; }
     static get properties() {
       return {
         nombre: {
           type: String
         },
         apellido: {
           type: String
         },
         id: {
           type: Number
         },
         ciudad: {
           type: String, /* comentario */
           value: "Desconocida"
         }
       };
     }
     mostrarDatos(datos) {
       console.log(datos.detail)
       console.log(datos.detail.response)
       this.nombre = datos.detail.response.nombre
       this.apellido = datos.detail.response.apellido
     }
   }

   window.customElements.define('visor-usuario', VisorUsuario);
