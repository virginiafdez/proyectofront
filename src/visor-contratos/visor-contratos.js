import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

  class VisorContratos extends PolymerElement {
    static get template() {
      return html`

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      :host {
        display: block;
        background-color: #E8F0F7
      }

      .pull-right {
         float: right !important;
      }

    </style>

    <iron-ajax id="obtenerContratos"
          url="http://localhost:3000/contratos/{{idcliente}}"
          handle-as="json"
          on-response="mostrarContratos"
          on-error="mostrarErrorContratos">
    </iron-ajax>

    <iron-ajax id="hacerLogout"
          url="http://localhost:3000/usuarios/logout"
          method="POST"
          handle-as="json"
          on-response="mostrarLogout"
          on-error="mostrarErrorLogout">
    </iron-ajax>

    <iron-ajax id="borradoContrato"
          url="http://localhost:3000/contratos/cancelar/{{iban}}"
          method="POST"
          handle-as="json"
          on-response="mostrarBorradoContrato"
          on-error="mostrarErrorBorradoContrato">
    </iron-ajax>

    <iron-ajax id="altaContrato"
          url="http://localhost:3000/contratos"
          method="POST"
          handle-as="json"
          on-response="mostrarAltaContrato"
          on-error="mostrarErrorAltaContrato">
    </iron-ajax>

    <button class="btn btn-primary pull-right" hidden$="{{!showlogout}}" on-click="contratar">Contratar</button>
    <br><br>

    <h1 align="center">Sus contratos</h1><br>

    <table class="table table-striped">
       <thead>
         <th>IBAN</th>
         <th>Saldo</th>
       </thead>
       <tbody>
           <template is="dom-repeat" items="{{contratos}}">
              <tr>
                <td>{{item.iban}}</td>
                <td>{{item.saldo}}</td>
                <td><button class="btn btn-info" on-click="consultarMovimientos" iban={{item.iban}}>Movimientos</button></td>
                <td><button class ="btn btn-primary" on-click="confirmarCancelar" iban={{item.iban}}>Cancelar Contrato</button></td>
          </template>
    </table>

    `;
  }

      static get is() { return 'visor-contratos'; }
      static get properties() {
        return {
          idcliente:{
            type: Number
          },
          contrato:{
            type: Array
          },
          iscontrato:{
            type: Boolean
          },
          nombre:{
            type: String
          },
          estado:{
            type: String,
          },
          iban:{
            type: String
          }
        };
      }

      mostrarContratos(resultado){
        this.iscontrato = true
        this.contratos=resultado.detail.response
      }

      //mostrarErrorContratos(error){
      mostrarErrorContratos(error){
        console.log("Se ha producido error")
        this.dispatchEvent(new CustomEvent("eventogetcontratosko", {"detail":{}}))
      }

      obtenerContratos(id){
        this.idcliente = id
        this.$.obtenerContratos.headers.estado = this.estado
        this.$.obtenerContratos.generateRequest()
      }

      consultarMovimientos(e){
        var iban = e.currentTarget.iban
        var idcliente = this.idcliente
        this.dispatchEvent(new CustomEvent("contratoconsultado", {"detail":{"iban":iban, "idcliente": idcliente}}))
      }

      confirmarCancelar(e){
        var iban = e.currentTarget.iban
        var idcliente = this.idcliente
        this.dispatchEvent(new CustomEvent("eventoconfirmarcancelar", {"detail":{"iban":iban}}))
      }

      contratar(){
        this.$.altaContrato.headers.idcliente = this.idcliente
        this.$.altaContrato.generateRequest()
      }

      mostrarAltaContrato(resultado){
        this.$.obtenerContratos.params.idcliente = this.idcliente
        this.$.obtenerContratos.headers.estado = this.estado
        this.$.obtenerContratos.generateRequest()
      }

      mostrarErrorAltaContrato(error){
        this.logado = false
      }

    }

    window.customElements.define('visor-contratos', VisorContratos);
