import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <style>
     :host {
       display: block;
       padding: 10px;
       background-color: #E8F0F7
     }

     .titulo-gris-26{
       color: grey;
       font-weight: bold;
       font-size: 26px
     }

     .titulo-blue-26{
       color: blue;
       font-weight: bold;
       font-size: 26px
     }

   </style>

   <iron-ajax id="hacerLogin"
         url="http://localhost:3000/usuarios/login"
         method="POST"
         handle-as="json"
         on-response="mostrarResultadoLogin"
         on-error="mostrarErrorLogin">
   </iron-ajax>

   <h1>Bienvenido a su BancoTechU</h1><br>

   <div>
       <div class="titulo-gris-26">Usuario registrado</div>
       <br>
       <div class="titulo-blue-26">Conéctate</div><br>
       <div>
          <input type="email" class="focus text-field" placeholder="Correo electrónico" autocomplete="off" value="{{email::input}}" required/><br><br>
          <input type="password" placeholder="Password" autocomplete="off" value="{{password::input}}" required/><br><br>
          <button class="btn btn-primary" on-click="logar">Login</button>
        </div>
        <br><br>
        <div class="titulo-gris-26">¿Nuevo usuario?<br>Regístrate</div>
        <br>
        <div class="titulo-blue-26">Para acceder a BancoTechU debe registrarse.<br>Solo te llevará unos minutos</div>
        <br>
        <div class="button">
           <button class="btn btn-primary" on-click="registrar">Regístrate</button>
        </div>
        </div>
        `;
    }

    static get is() { return 'login-usuario'; }
     static get properties() {
       return {
         email: {
           type: String
         },
         password: {
           type: String
         },
         logado:{
           type: Boolean,
           value: false
         },
         nombre:{
           type: String
         },
         idcliente:{
           type: Number
         }
       };
     }

     mostrarResultadoLogin(resultado){
       this.logado = true
       this.nombre = resultado.detail.response.nombre
       this.idcliente = resultado.detail.response.id
       sessionStorage.setItem("idcliente", this.idcliente)
       this.dispatchEvent(new CustomEvent("eventologado", {"detail":{"id":resultado.detail.response.id}}))
     }

     logar(){
       this.$.hacerLogin.headers.email = this.email
       this.$.hacerLogin.headers.password = this.password
       this.$.hacerLogin.generateRequest()
     }

     mostrarErrorLogin(error){
       this.logado = false
       this.dispatchEvent(new CustomEvent("eventoerrorlogado", {"detail":{}}))
     }

     registrar(){
       this.logado = false
       this.dispatchEvent(new CustomEvent("eventoregistro", {"detail":{}}))
     }

   }

   window.customElements.define('login-usuario', LoginUsuario);
