import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'


class AltaOk extends PolymerElement {
  static get template() {
    return html`
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <style>
      :host {
        display: block;
        background-color: #E8F0F7
      }
     </style>
    <h1>Alta realizada correctamente</h1><br>
    <button class="btn btn-primary" on-click="volverlogin">Volver a Login</button>

  `;
  }

     static get is() { return 'altaok'; }
      static get properties() {
        return {
        };
      }

      volverlogin(){
        this.dispatchEvent(new CustomEvent("eventovolverlogin", {"detail":{}}))
      }
    }

    window.customElements.define('alta-ok', AltaOk);
