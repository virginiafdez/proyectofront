import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

class ErrorLogado extends PolymerElement {
  static get template() {
    return html`
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <style>
     :host {
       display: block;
       padding: 10px;
       text-align: center;
       background-color: #E8F0F7
     }

     .titulo-negro-46 {
       font-family: sans-serif;
       font-size: 46px;
       font-weight: bold;
       color: black;
       text-align: left
     }

     .titulo-negro-36 {
       font-family: sans-serif;
       font-size: 36px;
       font-weight: bold;
       color: black;
       text-align: center
     }

     .pull-center {
        float: center !important;
     }
   </style>

   <div class="titulo-negro-46">Upps......</div><br><br>

   <div>
       <div style text-align="center">
            <p class="titulo-negro-36">Alguno de los datos introducidos no son correctos.</p><br>
            <!--<button style = "border:1px solid red; text-align=center;font-size=20" on-click="volverlogin">Volver a intentarlo</button>-->
            <button class="btn btn-primary pull-center" on-click="volverlogin">Volver a intentarlo</button>
        </div>
        <br><br>
    </div>
    `;
  }

     static get is() { return 'error-logado'; }
     static get properties() {
       return {
      };
     }

     volverlogin(){
       this.dispatchEvent(new CustomEvent("eventovolverlogin", {"detail":{}}))
     }
   }

   window.customElements.define('error-logado', ErrorLogado);
