import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

class VisorMovimientos extends PolymerElement {
  static get template() {
    return html`
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      :host {
        display: block;
        background-color: #E8F0F7
      }

      .pull-right {
         float: right !important;
      }
    </style>

    <iron-ajax id="obtenerMovimientos"
          url="http://localhost:3000/contratos/movimientos/{{iban}}"
          handle-as="json"
          on-response="mostrarMovimientos"
          on-error="mostrarErrorMovimientos">
    </iron-ajax>

    <button class="btn btn-info pull-right" hidden$="{{!showlogout}}" on-click="volvercontratos">Volver a Contratos</button><br>
    <br><br>
    <h2 align="center">Movimientos del Contrato {{iban}}</h2><br>

    <table class="table table-striped">
       <thead>
         <th>Fecha</th>
         <th>Concepto</th>
         <th>Importe</th>
       </thead>
       <tbody>
           <template is="dom-repeat" items="{{movimientos}}">
              <tr>
                <td>{{item.fecha}}</td>
                <td>{{item.concepto}}</td>
                <td>{{item.importe}}</td>
              </tr>
          </template>
    </table>
    <br>
    `;
  }

      static get is() { return 'visor-movimientos'; }
      static get properties() {
        return {
          iban:{
            type: String,
          },
          idcliente:{
            type: Number
          },
          movimientos:{
            type: Array
          }
        };
      }

      mostrarMovimientos(resultadomovimientos){
        this.movimientos=resultadomovimientos.detail.response[0].movimientos
      }

      mostrarErrorMovimientos(error){
        var idcliente = this.idcliente
        this.dispatchEvent(new CustomEvent("eventogetmvtosko", {"detail":{"idcliente":idcliente}}))
      }

      obtenerMovimientos(iban){
        this.iban = iban
        this.$.obtenerMovimientos.generateRequest()
      }

      volvercontratos(){
        this.dispatchEvent(new CustomEvent("eventovolvercontratos", {"detail":{}}))
      }
    }

    window.customElements.define('visor-movimientos', VisorMovimientos);
