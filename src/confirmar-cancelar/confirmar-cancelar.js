import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-ajax/iron-ajax.js'

class ConfirmarCancelar extends PolymerElement {
  static get template() {
    return html`

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
      :host {
        display: block;
        background-color: #E8F0F7
      }

      .h1{
        text-align: center
      }
    </style>

    <iron-ajax id="borradoContrato"
          url="http://localhost:3000/contratos/cancelar/{{iban}}"
          method="POST"
          handle-as="json"
          on-response="mostrarBorradoContrato"
          on-error="mostrarErrorBorradoContrato">
    </iron-ajax>

    <br><br>
    <h1>Se cancelará el Contrato {{iban}}</h1><br>
    <h2>Si está seguro pulse Confirmar</h2><br>
    <button class="btn btn-danger" on-click="cancelarContrato" iban={{iban}}>Confirmar</button>
    <button class="btn btn-info" on-click="backcontratos">Cancelar</button>


    `;
    }

      static get is() { return 'confirmar-cancelar'; }
      static get properties() {
        return {
          iban:{
            type: String
          },
          idcliente: {
            type: Number
          }
        };
      }

      cancelarContrato(e){
        var iban = e.currentTarget.iban
        this.iban = iban
        this.$.borradoContrato.generateRequest()
      }

      mostrarBorradoContrato(resultado){
        this.contratos=resultado.detail.response
        var idcliente = this.idcliente
        this.dispatchEvent(new CustomEvent("contratocancelado", {"detail":{"idcliente":idcliente}}))
      }

      mostrarErrorBorradoContrato(error){
        console.log("Se ha producido error en el borrado de contrato")
        console.log(error.detail.response)
      }

      backcontratos(){
        var idcliente = this.idcliente
        this.dispatchEvent(new CustomEvent("contratocancelado", {"detail":{"idcliente":idcliente}}))
      }

    }

    window.customElements.define('confirmar-cancelar', ConfirmarCancelar);
