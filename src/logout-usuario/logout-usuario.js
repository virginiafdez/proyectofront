import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'

class LogoutUsuario extends PolymerElement {
  static get template() {
    return html`
   <style>
     :host {
       display: block;
       padding: 10px;
       background-color: #E8F0F7
     }
   </style>

   <h1>Pagina de Logout</h1>
   <span hidden$="{{!logado}}">Hasta pronto {{nombre}}</td></span>
   `;
 }

     static get is() { return 'logout-usuario'; }
     static get properties() {
       return {
         nombre:{
           type: String
         }
       };
     }
   }

   window.customElements.define('logout-usuario', LogoutUsuario);
