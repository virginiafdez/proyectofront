import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';


class RegistroUsuario extends PolymerElement {
  static get template() {
    return html`
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <style>
     :host {
       display: block;
       padding: 10px;
       background-color: #E8F0F7
     }

     .titulo-blue-26{
       color: blue;
       font-weight: bold;
       font-size: 26px
     }
   </style>

   <iron-ajax id="altaRegistro"
         url="http://localhost:3000/usuarios"
         method="POST"
         handle-as="json"
         on-response="mostrarResultadoAlta"
         on-error="mostrarErrorAltaRegistro">
   </iron-ajax>

   <div class="titulo-blue-26">Introduce tus datos</div><br>
   <div>
       <div>
          <input type="text" class="focus text-field" placeholder="Nombre" autocomplete="off" value="{{nombre::input}}" required/><br><br>
          <input type="text" placeholder="Apellido" autocomplete="off" value="{{apellido::input}}" required/><br><br>
          <input type="text" placeholder="Ciudad" autocomplete="off" value="{{ciudad::input}}" required/><br><br>
          <input type="tel" placeholder="Telefono" autocomplete="off" value="{{telefono::input}}" required/><br><br>
          <input type="email" class="focus text-field" placeholder="Correo electrónico" autocomplete="off" value="{{email::input}}" required/><br><br>
          <input type="password" placeholder="Password" autocomplete="off" value="{{password::input}}" required/><br><br>
          <button class="btn btn-primary" on-click="altaregistro">Alta registro</button>
          <button class="btn btn-info" on-click="volverlogin">Volver a Login</button>
        </div>
        <br><br>
    </div>
    `;
  }

     static get is() { return 'registro-usuario'; }
     static get properties() {
       return {
         nombre: {
           type: String
         },
         apellido: {
           type: String
         },
         ciudad:{
           type: String
         },
         telefono:{
           type: String
         },
         email: {
           type: String
         },
         password: {
           type: String
         },
         alta:{
           type: Boolean,
           value: false
         }
      };
     }

     mostrarResultadoAlta(resultado){
       this.alta = true
       this.dispatchEvent(new CustomEvent("eventoaltaok", {"detail":{"id":resultado.detail.response.id}}))
     }

     altaregistro(){
       this.$.altaRegistro.headers.nombre = this.nombre
       this.$.altaRegistro.headers.apellido = this.apellido
       this.$.altaRegistro.headers.ciudad = this.ciudad
       this.$.altaRegistro.headers.telefono = this.telefono
       this.$.altaRegistro.headers.email = this.email
       this.$.altaRegistro.headers.password = this.password
       this.$.altaRegistro.generateRequest()
     }

     mostrarErrorAltaRegistro(error){
       this.logado = false
     }

     volverlogin(){
       this.dispatchEvent(new CustomEvent("eventovolverlogin", {"detail":{}}))
     }

   }

   window.customElements.define('registro-usuario', RegistroUsuario);
