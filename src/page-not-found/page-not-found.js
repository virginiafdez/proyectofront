import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

class PageNotFound extends PolymerElement {
static get template() {
  return html`
    <style>
      :host {
        display: block;
      }

      .h1{
        text-align: center
      }
    </style>
    <h1>Página no encontrada. Volver a la página principal</h1>
    `;
  }

      static get is() { return 'page-not-found'; }
      static get properties() {
        return {
        };
      }
    }

    window.customElements.define('page-not-found', PageNotFound);
